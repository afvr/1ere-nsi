# Jeu Motus  

Niveau de difficulté : :octicons-star-fill-16: :octicons-star-fill-16: :octicons-star-16: :octicons-star-16: :octicons-star-16: 


## Description du jeu 

MOTUS&reg; est une jeu télévisé français diffusé sur France 2 depuis le 25 juin 1990.

Un candidat propose un mot de 7 lettres dans un délai maximum de 8 secondes et doit épeler celui-ci.

Le mot doit contenir le bon nombre de lettres et doit être correctement orthographié, sinon il est refusé.

Le mot apparaît alors sur une grille : les lettres bien placées sont coloriées en rouge. Pour les lettres restantes, celle présentes mais mal placées sont cerclées de jaune.


## Description du projet

### L'essentiel

Dans un premier temps, le jeu est à deux joueurs humains.
Le joueur 1 choisi le mot de **7 lettres** à faire deviner, le joueur 2 cherche le mot.

!!! todo "A faire"

    Etant donné un mot à deviner, écrire une fonction qui donne la première lettre et demande au joueur 1 d'entrer une proposition jusqu'à ce qu'il trouve le mot.

    Après chaque proposition du joueur 2 une réponse est affichée de la manière suivante : 

    * Les lettres bien placées sont écrites en majuscules
    * Les lettres mal placées sont écrites en minuscules
    * Les autres sont remplacées par des points.

    Par exemple si le mot à trouver est `'voiture'` et que la proposition est `'vitrine'`, le programme doit afficher : `'Vitr..E'`

??? help "Aide Python"

    === "Lire et enregistrer une entrée clavier"

        `proposition = input("Entrer un mot : ")`


### Premières améliorations

!!! todo "A faire"

    Une fois que votre programme est opérationnel, vous devez l'améliorer avec les points suivants : 

    * Le joueur n'a que 6 essais. 
	* On affiche quand c'est gagné ou le mot si le joueur ne l'a pas trouvé. 

### Choix aléatoire du mot 

On veut maintenant que le mot soit choisi aléatoirement.

* Télécharger le fichier [aleamot.py]() 
* L'enregistrer dans le même dossier que votre projet
* Ajouter `from aleamot import *` au début de votre fichier Python
* L'appel `choix_mot()`renvoie un mot de 7 lettres du dictionnaire français.


### Aide



Si vous êtes bloqués pour l'affichage du résultat.

??? help "Aide"

    === "Lettre absente"

        A quelle condition une lettre de votre proposition de `mot` est-elle absente du mot `solution`?

    === "Lettre bien placée"

        A quelle condition une lettre est bien placée dans votre proposition de `mot` ?

    === "Lettre mal placée "

        A quelle condition une lettre est-elle mal placée dans votre proposition de `mot` ?

        Le mot à trouver est `'voiture'`, le mot proposé est `'tortue'` parmi les affichages suivants lequel est correct ? 

        - [ ] tOrtUE
        - [ ] tOr.UE
        - [ ] .OrtUE

        Utilisez un dictionnaire pour connaître les lettres **encore** 'diponibles'.