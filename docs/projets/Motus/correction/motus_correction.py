import aleamot

def affiche_resultat(proposition, solution):
    mot = proposition.lower()
    resultat = ''
    
    lettres = {}
    for l in solution :
        if l not in lettres :
            lettres[l] = 1
        else:
            lettres[l] = lettres[l] + 1
            
    for i in range(7):
        if mot[i] == solution[i] :
            resultat = resultat + mot[i].upper()
            lettres[mot[i]] = lettres[mot[i]] - 1
        elif mot[i] not in lettres or lettres[mot[i]] == 0:
            resultat = resultat + '.'
        else :
            resultat = resultat + mot[i]
            lettres[mot[i]] = lettres[mot[i]] - 1
            
    print(resultat)

def motus(solution):
    print(solution[0].upper()+ "......")
    nombre_essais_max = 6
    nombre_essais = 0
    mot = ""
    while nombre_essais <= nombre_essais_max and solution != mot :
        mot = input("Donner un mot de 7 lettres : ")
        if len(mot) != 7 :
            print("proposition invalide")
        else :
            affiche_resultat(mot, solution)
        nombre_essais = nombre_essais + 1
        
    

#motus(aleamot.choix_mot())
affiche_resultat('vitrine','voiture')


