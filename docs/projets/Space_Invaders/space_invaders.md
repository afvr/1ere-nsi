# Jeu Space Invaders  

Niveau de difficulté : :octicons-star-fill-16: :octicons-star-fill-16: :octicons-star-fill-16: :octicons-star-fill-16: :octicons-star-16:


__Prise en main du module Pyxel__

Le but de ce  projet guidé de créer un jeu type _Space Invaders_

1. Créer notre vaisseau
2. Créer des tirs
3. Créer des ennemis
4. Ajouter les collisions entre le vaisseau et les ennemis
5. Ajouter des effets : les explosions lors des collisions
6. Améliorer le visuel : ajouter des images

## Création de la fenêtre de jeu

Copier le code suivant. Tester-le : une fenêtre noire doit s'afficher.


```python
import pyxel

# =========================================================
# == VARIABLES GLOBALES
# =========================================================
# ici seront ajoutées au fur et à mesure les variables globales nécessaires.

# =========================================================
# == DIVERSES FONCTIONS
# =========================================================
#ici seront ajoutées au fur et à mesure diverses fonctions.

# =========================================================
# == UPDATE et DRAW essentielles
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""
    pass

def draw():
    """création des objets (30 fois par seconde)"""
	pass


# taille de la fenêtre 128x128 pixels.
pyxel.init(128, 128, title="Mon premier jeu")
# lancement du jeu
pyxel.run(update, draw)
```
Deux fonctions sont obligatoirement présentes : `update` et `draw`.
La fonction `update`  permet de mettre à jour l'état du jeu (30 fois par seconde) et la fonction `draw` permet de créer les éléments à afficher dans la fenêtre. \bigskip


Des éléments vont ensuite pouvoir être ajoutés dans la fenêtre. Le repère utilisé dans la fenêtre est le suivant :

* l'origine est le pixel en haut à gauche
* l'axe des abscisse est horizontale orientée vers la droite (plus le pixel est à droite de la fenêtre plus son abscisse est grande.)
* l'axe des ordonnées est verticale orientée vers le bas (plus le pixel est en bas de la fenêtre plus son ordonnée est grande)

## Gestion du vaisseau

### Création du vaisseau

Pour démarrer nous allons représenter notre vaisseau comme un carré de **8 pixels** de côté.

!!! question "Recherche"
    Rechercher dans la documentation Pyxel la commande pour créer un carré.


!!! note "A faire"
    Dans la fonction `draw`, ajouter la ligne permettant d'ajouter le vaisseau aux coordonnées $(60; 115)$

### Déplacement du vaisseau


Nous avons besoin de connaître à chaque instant la position du vaisseau.

!!! abstract "Variables globales"
    Ajouter deux variables globales :

    * `vaisseau_x` : abscisse du coin _haut gauche_ du vaisseau.
    * `vaisseau_y` : ordonnée du coin _haut gauche_ du vaisseau.

Notre vaisseau doit pouvoir se déplacer horizontalement et verticalement.

!!! note "A faire"
    Compléter la fonction suivante permettant de mettre à jour les coordonnées du vaisseau en fonction des touches directionnelles.

```python
def deplacement_vaisseau():
    """déplacement avec les touches de directions"""

    global vaisseau_x, vaisseau_y

    if pyxel.btn(pyxel.KEY_RIGHT):
        if vaisseau_x < 120 :
            vaisseau_x = vaisseau_x + 1
    if pyxel.btn(...):
        if vaisseau_x > 0 :
            vaisseau_x = ...
    if pyxel.btn(pyxel.KEY_DOWN):
        if ... :
            vaisseau_y = ...
    if pyxel.btn(...):
        if ... :
            vaisseau_y = ...
```

!!! warning "Point d'attention"
    les variables globales `vaisseau_x`, `vaisseau_y` doivent être spécifiée au début de la fonction avec la ligne
    `global vaisseau_x, vaisseau_y`

!!! note "A faire"
    Il faut ensuite ajouter un appel à la fonction `deplacement_vaisseau` dans la fonction `update`.  

    _Pour rappel cette fonction `update` est appelée 30 fois par seconde._


!!! danger "Exécution"
    1. Exécuter votre script.
    2. Quel problème rencontre-t-on ?
    3. Ajouter au début de la fonction `draw` la ligne suivante : `pyxel.cls(0)`? Règle-t-elle le problème ?

## Armement du vaisseau spatial

Si vous êtes bloqué voici le code obtenu jusqu'ici.

??? abstract "Solutions"     
    ```python
    import pyxel

    # taille de la fenetre 128x128 pixels
    # ne pas modifier
    pyxel.init(128, 128, title=" My Space Invaders ")

    # position initiale du vaisseau
    # (origine des positions : coin haut gauche)
    vaisseau_x = 60
    vaisseau_y = 115

    def deplacement_vaisseau(x, y):
      """déplacement avec les touches de directions"""

      if pyxel.btn(pyxel.KEY_RIGHT):
          if (x < 120) :
              x = x + 1
      if pyxel.btn(pyxel.KEY_LEFT):
          if (x > 0) :
              x = x - 1
      if pyxel.btn(pyxel.KEY_DOWN):
          if (y < 120) :
              y = y + 1
      if pyxel.btn(pyxel.KEY_UP):
          if (y > 0) :
              y = y - 1
      return x, y


    # =========================================================
    # == UPDATE
    # =========================================================
    def update():
      """mise à jour des variables (30 fois par seconde)"""

      global vaisseau_x, vaisseau_y

      # mise à jour de la position du vaisseau
      vaisseau_x, vaisseau_y = deplacement_vaisseau(vaisseau_x, vaisseau_y)


    # =========================================================
    # == DRAW
    # =========================================================
    def draw():
      """création des objets (30 fois par seconde)"""

      global vaisseau_x, vaisseau_y

      # vide la fenetre
      pyxel.cls(0)

      # dessin du vaisseau (carre 8x8)
      pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

      pyxel.run(update, draw)
    ```

Nous allons ajouter les tirs du vaisseau.
- la touche `espace` permet de lancer un tir
- les tirs du vaisseau seront représentés (dans un premier temps) par un rectangle jaune d'un pixel de large et de quatre pixels de haut.
- Chaque tir se déplace verticalement jusqu'à sortir de la fenêtre du jeu.

### Création des tirs

1. Trouver dans la documentation, la différence entre les méthodes `pyxel.btn` et `pyxel.btnr`.
2. Un vaisseau a une réserve de munitions illimitée. Plusieurs tirs peuvent être actifs dans le jeu.
    - Dans la fonction `update` les coordonnées des tirs sont mis à jours.
    - Dans la fonction `draw` les tirs sont dessinés.


    !!! abstract "Variables globales"
        Il  faut avoir la connaissance des tirs actifs avec leur position.

        `les_tirs_actifs` : liste de couple $(x,y)$ correspondant aux coordonnées du coin supérieur gauche du tir à dessiner.

    !!! note "A faire"
        1. Créer une fonction `creation_tirs` qui prend en paramètres des coordonnées $x$ et $y$ qui ajoute le couple $(x+4;y+4)$ à la liste `les_tirs_actifs` lorsque la touche `ESPACE` est pressée.
        2. Mettre à jour en conséquence la fonction `update` correspondant aux commentaires suivants :
        ```python
        # creation des tirs en fonction de la position du vaisseau
        # mise à jour des positions des tirs
        ```

        A ce stade rien de se passe, seule la liste globale `les_tirs_actifs` s'enrichie.

        3. Mettre à jour la fonction `draw`pour voir apparaître les tirs avec le commentaire :
        ```python
        # dessin des tirs (rectangle 1x4 pixels)
        ```





    !!! danger "Exécution"
        Actuellement vos tirs sont immobiles. L'étape suivante est de les faire se déplacer automatiquement vers le haut de l'écran.

### Déplacement des tirs

!!! note "A faire"
    Créer une fonction `deplacement_tirs` qui permet de déplacer chaque tir vers le haut.

    Les tirs qui ne sont plus visibles dans la fenêtre de jeu doivent être supprimés de la liste `les_tirs_actifs`

    ??? tip "Aide"
        `les_tirs_actifs.remove(tir)` supprime l'élément `tir` de la liste `les_tirs_actifs`.


## Attaque ennemie

Si vous êtes bloqué, vous pouvez copier le script ci-desous

??? abstract "Solution"
    ```python
    import pyxel

    # taille de la fenetre 128x128 pixels
    # ne pas modifier
    pyxel.init(128, 128, title=" My Space Invaders ")

    # position initiale du vaisseau
    # (origine des positions : coin haut gauche)
    vaisseau_x = 60
    vaisseau_y = 115

    # initialisation des tirs
    les_tirs_actifs = []

    def deplacement_vaisseau(x, y):
    """déplacement avec les touches de directions"""

    if pyxel.btn(pyxel.KEY_RIGHT):
        if (x < 120) :
            x = x + 1
    if pyxel.btn(pyxel.KEY_LEFT):
        if (x > 0) :
            x = x - 1
    if pyxel.btn(pyxel.KEY_DOWN):
        if (y < 120) :
            y = y + 1
    if pyxel.btn(pyxel.KEY_UP):
        if (y > 0) :
            y = y - 1
    return x, y

    def creation_tirs(x, y):
    """création d'un tir avec la barre d'espace"""

    global les_tirs_actifs

    # btnr pour eviter les tirs multiples
    if pyxel.btnr(pyxel.KEY_SPACE):
        les_tirs_actifs.append([x+4, y-4])


    def deplacement_tirs():
    """déplacement des tirs vers le haut et suppression s'ils sortent du cadre"""

    global les_tirs_actifs

    for tir in les_tirs_actifs:
        tir[1] -= 1
        if  tir[1]<-8:
            les_tirs_actifs.remove(tir)

    # =========================================================
    # == UPDATE
    # =========================================================
    def update():
    """mise à jour des variables (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y, les_tirs_actifs

    # mise à jour de la position du vaisseau
    vaisseau_x, vaisseau_y = deplacement_vaisseau(vaisseau_x, vaisseau_y)

    # creation des tirs en fonction de la position du vaisseau
    creation_tirs(vaisseau_x, vaisseau_y)

    # mise a jour des positions des tirs
    deplacement_tirs()





    # =========================================================
    # == DRAW
    # =========================================================
    def draw():
    """création des objets (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y, les_tirs_actifs

    # vide la fenetre
    pyxel.cls(0)

    # dessine le vaisseau (carre 8x8 de couleur violet)
    pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

    # dessine les tirs (rectangle 1x4 de couleur jaune)
    for tir in les_tirs_actifs:
        pyxel.rect(tir[0], tir[1], 1, 4, 10)

    pyxel.run(update, draw)

    ```


On veut faire apparaître, de manière aléoitre, des ennemis arrivant du haut de la fenêtre et descendant verticalement.

Ils sont représentés par un carré de 8 pixels de côté et de couleur rose.

Un nouvel ennemi apparaît à chaque seconde, il meurt lorsqu'il n'apparaît plus dans la fenêtre de jeu (la mort par collision avec un tir se fera dans la partie suivante).

!!! question "Recherche"
    Se renseigner sur la méthode `pyxel.frame_count`

Le fonctionnenement des ennemis est très semblable à celui des tirs réalisé précédemment.

!!! success "A faire"
    Une collection de fonctions et la mise à jour des fonctions `update` et `draw` pour faire apparaître et se déplacer jusqu'en bas de la fenêtre les ennemis.

??? note "Besoin d'être accompagné comme pour les tirs"

    !!! abstract "Variables globales"
        Comme pour les tirs, il peut y avoir plusieurs ennemis dans la fenêtre de jeu. Il va donc falloir une nouvelle variable globale.

        Ajouter la variable globale :

        - `les_ennemis` une liste de couple $(x;y)$ correspondant aux coordonnées du coin supérieur gauche de chaque ennemi.

    !!! note "A faire"
        Sur le modèle des tirs :

        - Une fonction `creation_ennemi` qui permet de créer un ennemi par seconde positionné aléatoirement en haut de la fenêtre.
        - Une fonction `deplacement_ennemis` qui permet le déplacement de chaque ennemi verticalement vers le bas et les supprime lorsqu'ils sortent de la fenêtre de jeu.
        - Mettre à jour les fonctions `update` et `draw`

    !!! danger "Exécution"
        Actuellement, vos ennemis descendent, aucune action n'est réalisée si un tir ou le vaisseau entre en collision avec un ennemi.

## Gestion des collions

## Améliorations graphiques

### Des explosions lors des collisions

### Des images à la place des rectangle de couleurs !

## Ambiance musicale
